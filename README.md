This repository contains scripts and code for executing the analyses described in my submission to the 2015 AMIA Translational Summit - Design Challenge.

The raw data is not located in this repository. However, the tidy data is. The KEGG NSCLC genes were downloaded via the Molecular Signatures Database. The gene/drug/disease relationships were obtained from https://www.synapse.org/#!Synapse:syn3095778.

Please contact [Stephen Piccolo](https://piccolo.byu.edu) with questions.

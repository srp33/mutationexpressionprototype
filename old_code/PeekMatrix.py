import os, sys, glob
import utilities

inFilePath = sys.argv[1]
numRows = int(sys.argv[2])
numCols = int(sys.argv[3])

outFilePath = ""
if len(sys.argv) > 4:
    outFilePath = sys.argv[4]

outData = []

rowCount = 0
for line in file(inFilePath):
    row = line.rstrip().split("\t")
    rowCount += 1

    if numCols > 0:
        row = row[:numCols]

    outData.append(row)

    if row > 0 and rowCount == numRows:
        break

if outFilePath == "":
    for row in outData:
        print row
else:
    utilities.writeMatrixToFile(outData, outFilePath)

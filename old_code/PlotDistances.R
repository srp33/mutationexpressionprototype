library(vioplot)

inFilePaths = strsplit(commandArgs()[7], ",")[[1]]
mains = strsplit(commandArgs()[8], ",")[[1]]
outFilePath = commandArgs()[9]

getAnovaPValue = function(l)
{
  values = unlist(l, use.names=F)

  factorValues = NULL
  for (n in names(l))
    factorValues = c(factorValues, rep(n, length(l[[n]])))

  aov.result = aov(values ~ as.factor(factorValues))
  aov.p = summary(aov.result)[[1]][[5]][1]
  aov.p = format(aov.p, digits=3)

  return(aov.p)
}

dataList = list()
for (i in 1:length(inFilePaths))
{
  inFilePath = inFilePaths[i]
  main = gsub(" vs ", " vs\n", mains[i])
  dataList[[main]] = as.vector(as.matrix(read.table(inFilePath, sep="\t", stringsAsFactors=F, header=T, row.names=1, check.names=F)))
  dataList[[main]] = dataList[[main]][which(dataList[[main]] > 0)]
}

ylim = c(min(unlist(dataList)), max(unlist(dataList)))

pdf(outFilePath, width=5.0, height=6)
par(mar=c(5, 5, 2, 0.1))
main = paste("p =", getAnovaPValue(dataList))
#main = ""
boxplot(dataList, ylim=ylim, main=main, ylab="Euclidean distance between samples", xaxt="n", yaxt="n", range=0)
axis(1, at=1:length(dataList), labels=names(dataList), mgp=c(3, 1.7, 0))
axis(2, mgp=c(3, 1.7, 0))


#par(mfrow=c(1,length(dataList)))
#for (i in 1:length(dataList))
#{
#  main = gsub(" vs ", " vs\n", mains[i])
#
#  boxplot(dataList[[i]], ylim=ylim, main=gsub(" vs ", " vs\n", main))
#
#  #vioplot(dataList[[inFilePaths[i]]], ylim=ylim, col="white", names=main)
#  #title(main)
#}
graphics.off()

#for (i in 2:length(inFilePaths))
#  print(t.test(dataList[[inFilePaths[1]]], dataList[[inFilePaths[i]]]))$p.value

import os, sys, glob

inFilePath = sys.argv[1]
index = int(sys.argv[2])
outFilePath = sys.argv[3]

numHeaderLines = 0
if len(sys.argv) > 4:
    numHeaderLines = int(sys.argv[4])

inFile = open(inFilePath)
outFile = open(outFilePath, 'w')

while numHeaderLines > 0:
    outFile.write(inFile.readline())
    numHeaderLines -= 1

for line in inFile:
    lineItems = line.rstrip().split("\t")
    lineItems[index] = lineItems[index][:12]
    outFile.write("\t".join(lineItems) + "\n")

outFile.close()
inFile.close()

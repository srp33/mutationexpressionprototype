import os, sys, glob
from utilities import *
import numpy as np
from sklearn.cluster import KMeans

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

data = readMatrixFromFile(inFilePath)
data.pop(0)

sampleIDs = []
for i in range(len(data)):
    sampleIDs.append(data[i].pop(0))
    data[i] = np.array([float(x) for x in data[i]])

nData = np.array(data)

print "Fitting clusters..."
result = KMeans(n_clusters=6, random_state=33).fit(nData)

outFile = open(outFilePath, 'w')
for i in range(len(sampleIDs)):
    outFile.write("%s\t%i\n" % (sampleIDs[i], result.labels_[i] + 1))
outFile.close()

print "Cluster frequency:"
print getItemFrequencyMap(result.labels_)

import os, sys, glob
from utilities import *

referenceDataFilePath = sys.argv[1]
testDataFilePath = sys.argv[2]
outFilePath = sys.argv[3]

def getDataDict(filePath):
    dataRaw = readMatrixFromFile(filePath)

    dataDict = {}
    for row in dataRaw[1:]:
        sample = row.pop(0)
        dataDict[sample] = [float(x) for x in row]

    return dataDict

referenceDict = getDataDict(referenceDataFilePath)
testDict = getDataDict(testDataFilePath)

referenceSamples = sorted(referenceDict.keys())
testSamples = sorted(testDict.keys())

if len(referenceSamples) < 5:
    print "Not sufficient samples in %s" % referenceDataFilePath
    exit(0)
if len(testSamples) < 5:
    print "Not sufficient samples in %s" % testDataFilePath
    exit(0)

outFile = open(outFilePath, 'w')
outFile.write("\t".join([""] + referenceSamples) + "\n")

for i in range(len(testSamples)):
    testSample = testSamples[i]
    print "%s (%i/%i)" % (testSample, i+1, len(testSamples))
    outRow = [testSample]

    for referenceSample in referenceSamples:
        distance = calculateEuclideanDistance(testDict[testSample], referenceDict[referenceSample])
        outRow.append("%.6f" % distance)

    outFile.write("\t".join(outRow) + "\n")

outFile.close()

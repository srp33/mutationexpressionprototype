import os, sys, glob, uuid, random, math, collections
import utilities

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

dataDict = {}
for line in file(inFilePath):
    lineItems = line.rstrip().split("\t")
    dataDict[lineItems[0]] = dataDict.setdefault(lineItems[0], []) + [lineItems[1]]

for key in dataDict:
    dataDict[key] = list(set(dataDict[key]))

    if len(dataDict[key]) > 1:
        print "Non-equal duplicates in %s: (%s = %s)" % (inFilePath, key, dataDict[key])
        exit(1)

outData = []
for key in sorted(dataDict.keys()):
    outData.append([key, dataDict[key][0]])

utilities.writeMatrixToFile(outData, outFilePath)
